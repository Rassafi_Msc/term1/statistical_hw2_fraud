import tensorflow as tf
import pandas as pd
import numpy as np
import time
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import accuracy_score as acc
from sklearn.metrics import confusion_matrix
from sklearn.metrics import roc_curve, roc_auc_score, classification_report, accuracy_score, confusion_matrix
import matplotlib.pyplot as plt
import seaborn as sns

credit_card = pd.read_csv('dataset/creditcard.csv')
#%%
X = credit_card.drop(columns='Class', axis=1)
y = credit_card.Class.values
#%%
np.random.seed(42)
X_train, X_test, y_train, y_test = train_test_split(X, y)
#%%
scaler = StandardScaler()
train_data, train_target = np.asmatrix(scaler.fit_transform(X_train)), y_train.reshape(-1, 1)
test_data, test_target = np.asmatrix(scaler.fit_transform(X_test)), y_test.reshape(-1, 1)

#parameters 
learning_rate = 0.001
epoch = 500
batch_size = 40000

#tensorflow model
x = tf.placeholder(tf.float64, shape=(None, 30), name="x")#30 feature
y = tf.placeholder(tf.float64, shape=(None, 1), name="y")
w = tf.Variable(tf.random_normal(shape=[30, 1], stddev=0.1, dtype=tf.float64), 
                name="weights", dtype=tf.float64)
b = tf.Variable(0.0, name="bias", dtype=tf.float64)

logit = tf.matmul(x, w) + b
y_predicted = 1.0/(1.0 + tf.exp(-logit))
loss = tf.reduce_sum(tf.nn.sigmoid_cross_entropy_with_logits(labels=y, logits=logit))
#loss = -1 * tf.reduce_sum(y * tf.log(y_predicted) + (1 - y) * tf.log(1 - y_predicted))
optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate).minimize(loss)

ses = tf.InteractiveSession()
ses.run(tf.global_variables_initializer())
train_loss_list = []
valid_loss_list = []
train_acc_list = []
valid_acc_list = []
st = time.time()
for e_itr in range(epoch):
    train_loss = 0
    start = time.time()
    for indx  in range(len(train_data)//batch_size):
        input_list = {x: train_data[indx * batch_size:(indx + 1) * batch_size],
                      y: train_target[indx * batch_size:(indx + 1) * batch_size]}
        _, tl = ses.run([optimizer, loss], feed_dict=input_list)
        train_loss += tl
    print(train_loss, "time = ", time.time()-start)
print("full time is = ", time.time()-st)

predict_test_un_round = ses.run(y_predicted, feed_dict={x: test_data})
fpr, tpr, thresholds = roc_curve(test_target, predict_test_un_round
                                 , drop_intermediate=True)
f, ax = plt.subplots(figsize=(9, 6))
_ = plt.plot(fpr, tpr, [0,1], [0, 1])
_ = plt.title('AUC ROC')
_ = plt.xlabel('False positive rate')
_ = plt.ylabel('True positive rate')
plt.savefig("roct.pdf")
train_auc_roc = roc_auc_score(test_target, predict_test_un_round)*100
print('Training AUC: %.4f %%' % train_auc_roc)

predict_test = (predict_test_un_round > 0.05) *1
print('Confusion matrix with 0.05 threshold:\n', confusion_matrix(test_target, predict_test))
print(classification_report(test_target, predict_test, digits=3))

train_accuracy = accuracy_score(test_target, predict_test)*100
print('Training accuracy: %.4f %%' % train_accuracy)


ses.close()

