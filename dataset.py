import numpy as np
import seaborn as sns
sns.set(style='whitegrid')
import pandas as pd
import matplotlib.pyplot as plt
credit_card = pd.read_csv('dataset/creditcard.csv')

#Dataset visualization

print(sum(credit_card['Class']))
print('Dataset Shape =>', credit_card.shape)
f, ax = plt.subplots(figsize=(7, 5))
sns.countplot(x='Class', data=credit_card, hue="Class")
plt.title('Fraud vs NonFraud')
plt.savefig("fnf.pdf")
plt.show()

corr = credit_card.corr()
mask = np.zeros_like(corr, dtype=bool)
mask[np.triu_indices_from(mask)] = True
cmap = sns.diverging_palette(500, 10, as_cmap=True)
f, ax = plt.subplots(figsize=(11, 9))
sns.heatmap(corr, mask=mask, cmap=cmap, vmax=0.2, center=0, square=False, linewidths=0.5,
            cbar_kws={"shrink": 0.5})
plt.savefig("hm.pdf")
