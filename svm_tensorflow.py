import tensorflow as tf
import pandas as pd
import numpy as np
import time
from sklearn.metrics import accuracy_score as acc
from sklearn.metrics import confusion_matrix
from sklearn.metrics import roc_curve, roc_auc_score, classification_report, accuracy_score, confusion_matrix
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

print("Loading data set please wait ... ")

credit_card = pd.read_csv('dataset/creditcard.csv')
#%%
X = credit_card.drop(columns='Class', axis=1)
y = credit_card.Class.values
#%%
np.random.seed(42)
X_train, X_test, y_train, y_test = train_test_split(X, y)
#%%
scaler = StandardScaler()
train_data, train_target = np.asmatrix(scaler.fit_transform(X_train)), y_train.reshape(-1, 1)
test_data, test_target = np.asmatrix(scaler.fit_transform(X_test)), y_test.reshape(-1, 1)
train_target = 2*train_target - 1
test_target = 2*test_target - 1
#parameters
learning_rate = 0.0005
epoch = 500
batch_size = 40000

#tensorflow model
x = tf.placeholder(tf.float32, shape=(None, 30), name="x")#30 feature
y = tf.placeholder(tf.float32, shape=(None, 1), name="y")
w = tf.Variable(tf.random_normal(shape=[30, 1], stddev=0.1, dtype=tf.float32),
                name="weights", dtype=tf.float32)
b = tf.Variable(0.0, name="bias", dtype=tf.float32)

#svm loss
model_output = tf.subtract(tf.matmul(x, w), b)
l2_norm = tf.reduce_sum(tf.square(w))
alpha = tf.constant([0.5], dtype=tf.float32)
classification_term = tf.reduce_sum(tf.maximum(0., tf.subtract(1.,tf.multiply(model_output, y))))
loss = tf.add(classification_term, tf.multiply(alpha, l2_norm))
optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate).minimize(loss)

ses = tf.InteractiveSession()
ses.run(tf.global_variables_initializer())
train_loss_list = []
valid_loss_list = []
train_acc_list = []
valid_acc_list = []
st = time.time()
for e_itr in range(epoch):
    train_loss = 0
    start = time.time()
    for indx  in range(len(train_data)//batch_size):
        input_list = {x: train_data[indx * batch_size:(indx + 1) * batch_size],
                      y: train_target[indx * batch_size:(indx + 1) * batch_size]}
        _, tl = ses.run([optimizer, loss], feed_dict=input_list)
        train_loss += tl
    print(train_loss, "time = ", time.time()-start)
print("full time is = ", time.time()-st)


test_target = (test_target + 1)/2
predict_test = ses.run(model_output, feed_dict={x: test_data})
fpr, tpr, thresholds = roc_curve(test_target, predict_test, drop_intermediate=True)
f, ax = plt.subplots(figsize=(9, 6))
_ = plt.plot(fpr, tpr, [0,1], [0, 1])
_ = plt.xlabel('False positive rate')
_ = plt.ylabel('True positive rate')
plt.style.use('seaborn')
plt.savefig('rocsvmt.pdf')

threshold = 0.5
predict_test[predict_test <= threshold] = 0
predict_test[predict_test > threshold] = 1
print('Confusion matrix with {} threshold:\n'.format(threshold),
      confusion_matrix(np.array(test_target), np.array(predict_test)))
print(classification_report(test_target, predict_test, digits=3))

predict_test_un_round = ses.run(model_output, feed_dict={x: test_data})
test_auc_roc = roc_auc_score(test_target, predict_test_un_round)*100
print('Training AUC: %.4f %%' % test_auc_roc)
